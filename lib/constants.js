export const EXAMPLE_PATH = "my-blog";
export const CMS_NAME = "My Blog";
export const CMS_DESCRIPTION = "This is my blog description";
export const HOME_OG_IMAGE_URL = "https://choosinghopeinthejourney.com/wp-content/uploads/2018/10/irls.png";
export const MOMENT_FORMAT_DATEANDTIME = "DD MMM YYYY h:mm A";
export const MOMENT_FORMAT_DATE = "DD MMM YYYY";

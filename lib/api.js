import data from "./data.json";

export function getPostBySlug(id, fields = []) {
  const posts = data.data;
  const obj = posts.find((e) => e.id == id);
  const items = {};
  fields.forEach((field) => {
    if (obj[field]) {
      items[field] = obj[field];
    }
  });

  return items;
}

export function getAllPosts() {
  const posts = data.data.sort((post1, post2) =>
    post1.date > post2.date ? "-1" : "1"
  );
  return posts;
}

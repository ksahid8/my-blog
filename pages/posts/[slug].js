import { useEffect, useState } from "react";
import PostBody from "../../components/post-details/post-body";
import PostHeader from "../../components/post-details/post-header";
import Layout from "../../components/partials/layout";
import Spinner from "../../components/partials/spinner";
import { getPostBySlug, getAllPosts } from "../../lib/api";
import Head from "next/head";
import Tags from "./../../components/tags";
import PostFooter from "../../components/post-details/post-footer";

export default function Post({ post }) {
  const [IsSpinner, setIsSpinner] = useState(true);

  const OverlayLoader = () => <Spinner />;

  useEffect(() => {
    if (post.title !== "") {
      setTimeout(() => {
        setIsSpinner(false);
      }, 500);
    } else {
      setIsSpinner(true);
    }
  }, [post.title]);

  return (
    <Layout>
      <>
        {IsSpinner && <OverlayLoader />}
        <article className="post-details">
          <Head>
            <title>{post.title}</title>
            <meta property="og:description" content={post.description} />
            <meta property="og:image" content={post.ogImage.url} />
          </Head>
          <PostHeader
            title={post.title}
            coverImage={post.coverImage}
            date={post.date}
          />
          <PostBody content={post.content} />
          <PostFooter />
          <Tags tags={post.tags} />
        </article>
      </>
    </Layout>
  );
}

export async function getStaticProps({ params }) {
  const post = getPostBySlug(params.slug, [
    "title",
    "description",
    "date",
    "id",
    "content",
    "ogImage",
    "coverImage",
    "tags",
  ]);
  return {
    props: {
      post: post,
    },
  };
}

export async function getStaticPaths() {
  const posts = getAllPosts(["id"]);
  return {
    paths: posts.map((post) => {
      return {
        params: {
          slug: post.id,
        },
      };
    }),
    fallback: false,
  };
}

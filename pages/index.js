import MorePosts from "../components/more-posts";
import StyleOne from "../components/style-one/style-one";
import Layout from "../components/partials/layout";
import { getAllPosts } from "../lib/api";
import Head from "next/head";
import { CMS_NAME } from "../lib/constants";
import { Row } from "react-bootstrap";
export default function Index({ allPosts }) {
  const styleOnePost = allPosts[0];
  const morePosts = allPosts.slice(1);

  return (
    <>
      <Layout>
        <Head>
          <title>{CMS_NAME}</title>
        </Head>
        <Row>
          {styleOnePost && (
            <StyleOne
              title={styleOnePost.title}
              coverImage={styleOnePost.coverImage}
              date={styleOnePost.date}
              slug={styleOnePost.id}
              description={styleOnePost.description}
              tags={styleOnePost.tags}
            />
          )}
          {morePosts.length > 0 && <MorePosts posts={morePosts} />}
        </Row>
      </Layout>
    </>
  );
}

export async function getStaticProps() {
  const allPosts = getAllPosts([
    "title",
    "date",
    "id",
    "coverImage",
    "description",
    "tags"
  ]);

  return {
    props: { allPosts },
  };
}

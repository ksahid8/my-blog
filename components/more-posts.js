import StyleTwo from "./style-two/style-two";

const MorePosts = ({ posts }) => {
  return (
    <>
      {posts.map((post) => (
        <StyleTwo
          key={post.id}
          title={post.title}
          coverImage={post.coverImage}
          date={post.date}
          slug={post.id}
          description={post.description}
          tags={post.tags}
        />
      ))}
    </>
  );
};
export default MorePosts;

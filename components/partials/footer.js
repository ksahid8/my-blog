import Container from "./container";

const Footer = () => {
  return (
    <footer>
      <Container>
        <div>
          <p>© 2020 My Blog, Inc. All Rights Reserved  </p>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTag } from "@fortawesome/free-solid-svg-icons";
import data from "./../../lib/tags.json";
import Link from "next/link";

const SidebarTags = () => {
  const Tags = data.tags;

  const TagList = Tags.map((key) => {
    return (
      <li className="d-flex" key={key.title}>
        <FontAwesomeIcon icon={faTag} />
        <Link as={`/posts/tags/${key.title}`} href="/posts/tags/[tag]">
          {key.title}
        </Link>
      </li>
    );
  });
  return (
    <div className="sidebar-tags">
      <h5 className="rightbar-title">Tags</h5>
      <div className="sidebar-container borders">
        <ul className="sidebar-list">{TagList}</ul>
      </div>
    </div>
  );
};
export default SidebarTags;

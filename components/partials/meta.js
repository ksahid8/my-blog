import Head from "next/head";
import { CMS_DESCRIPTION, HOME_OG_IMAGE_URL } from "../../lib/constants";

const Meta = () => {
  return (
    <Head>
      <meta property="og:description" content={`${CMS_DESCRIPTION}`} />
      <meta property="og:image" content={HOME_OG_IMAGE_URL} />
    </Head>
  );
};

export default Meta;

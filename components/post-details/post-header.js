import PostImage from "./post-image";
import moment from "moment";
import MomentConfig from "../../lib/moment-config";

const PostHeader = ({ title, coverImage, date }) => {
  return (
    <>
      <h3 className="mb-0">{title}</h3>
      <div className="max-w-2xl mx-auto">
        <div className="blog-time mb-3">
          {moment(date).fromNow()},{" "}
          {moment(date).format(MomentConfig.MOMENT_FORMAT_DATEANDTIME)}
        </div>
      </div>
      <div className="blog-detail-image-holder">
        <PostImage title={title} src={coverImage} />
      </div>
    </>
  );
};
export default PostHeader;

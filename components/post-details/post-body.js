const PostBody = ({ content }) => {
  return (
    <div className="max-w-2xl mx-auto">
      <div className="mt-3" dangerouslySetInnerHTML={{ __html: content }} />
    </div>
  );
};
export default PostBody;
